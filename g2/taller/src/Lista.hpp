#include "Lista.h"

Lista::Lista() : _head(nullptr), _tail(nullptr), _long(0) {}

Lista::Lista(const Lista &l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    while (_long > 0){
        Nodo* x = _head->_next;
        delete _head;
        _head = x;
        _long--;
    }
    _tail = nullptr;
}

Lista &Lista::operator=(const Lista &aCopiar) {
    if(_long >0){
        for (int i = _long - 1 ; i > -1 ; --i) {
            eliminar(i);
        }
    }
    for (int i = aCopiar._long -1; i > -1 ; --i) {
        this->agregarAdelante(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int &elem) {
    Nodo* nod = new Nodo(elem);
    if (_head != nullptr){
        (*_head)._prev = nod;
        (*nod)._next   = _head;
        _head = nod;
    }else{
        _head = nod;
        _tail = nod;
    }
    _long++;
}

void Lista::agregarAtras(const int &elem) {
    Nodo* nod = new Nodo(elem);
    if (_tail != nullptr){
        (*_tail)._next = nod;
        (*nod)._prev   = _tail;
        _tail = nod;
    }else{
        _head = nod;
        _tail = nod;
    }
    _long++;
}

void Lista::eliminar(Nat i) {
    assert(i<longitud());
    Nodo* x = _head;
    for (int j = 0; j < i; ++j) {
        x = (*x)._next;
    }
    if(x->_prev == nullptr){
        _head = x->_next;
    }else{
        (x->_prev)->_next = x->_next;
    }
    if(x->_next == nullptr){
        _tail = x->_prev;
    } else{
        (x->_next)->_prev = x->_prev;
    }
    _long--;
    delete x;
}

int Lista::longitud() const {
    return _long;
}

const int &Lista::iesimo(Nat i) const {
    assert(i < longitud());
    Nodo* x = _head;
    for (int j = 0; j < i ; ++j) {
        x = (*x)._next;
    }
    return (*x)._dato;
}

int &Lista::iesimo(Nat i) {
    assert(i<longitud());
    Nodo* x = _head;
    for (int j = 0; j < i ; ++j) {
        x = (*x)._next;
    }
    return (*x)._dato;
}

void Lista::mostrar(ostream &o) {
    Nodo* x = _head;
    o <<"{";
    for (int i = 0; i < _long; ++i) {
        o << x->_dato;
        if(i <_long-1){
            o << ",";
        }
        x = x->_next;
    }
    o<<"}";
}


Lista::Nodo::Nodo(int dato) : _dato(dato), _next(nullptr), _prev(nullptr) {}
