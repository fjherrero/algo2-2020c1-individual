
#include "Conjunto.h"

template<class T>
Conjunto<T>::Conjunto() : _raiz(nullptr), _cardinal(0) {}

template<class T>
Conjunto<T>::~Conjunto() {
    if (_raiz != nullptr) {
        if(_raiz->izq != nullptr) {
            eliminarNodo(_raiz->izq);
        }
        if(_raiz->der != nullptr) {
            eliminarNodo(_raiz->der);
        }
        delete _raiz;
        _cardinal--;
    }
}

template<class T>
bool Conjunto<T>::pertenece(const T &clave) const {
    bool r = false;
    Nodo *aux = _raiz;
    while (!r && aux != nullptr) {
        if (aux->valor == clave) {
            r = true;
        } else {
            if (aux->valor < clave) {
                aux = aux->der;
            } else {
                aux = aux->izq;
            }
        }
    }
    return r;
}

template<class T>
void Conjunto<T>::insertar(const T &clave) {
    if (!pertenece(clave)) {
        Nodo *aux = _raiz;
        Nodo *x = new Nodo(clave);
        if (aux == nullptr) {
            _raiz = x;
        } else {
            bool seIncerto = false;
            while (!seIncerto) {
                if (aux->valor < clave) {
                    if (aux->der != nullptr) {
                        aux = aux->der;
                    } else {
                        aux->der = x;
                        seIncerto = true;
                    }
                } else {
                    if (aux->izq != nullptr) {
                        aux = aux->izq;
                    } else {
                        aux->izq = x;
                        seIncerto = true;
                    }
                }
            }
        }
        _cardinal++;
    }
}

template<class T>
void Conjunto<T>::remover(const T &clave) {
    //Puntero hacia la raiz el nodo a remover
    if (pertenece(clave)) {
        Nodo *aux_0 = _raiz;
        //Puntero al nodo a remover
        Nodo *aux_1 = _raiz;
        //Busco el nodo a remover
        while (aux_1->valor != clave) {
            // if) Desplazamiento a la derecha / else) Desplazamiento a la izquierda
            if (aux_1->valor < clave) {
                aux_0 = aux_1;
                aux_1 = aux_1->der;
            } else {
                aux_0 = aux_1;
                aux_1 = aux_1->izq;
            }
        }
        //Si el nodo a remover no es la raiz del arbol, entonces busca su sucesor entre las ramas.
        // aux_2 es un puntero a la raiz del sucesor
        Nodo *aux_2 = aux_1;
        // aux_3 es un puntero al sucesor de aux_1
        Nodo *aux_3 = aux_1;
        //Busca el maximo de los minimos
        if (aux_3->izq != nullptr) {
            aux_3 = aux_3->izq;
            while (aux_3->der != nullptr) {
                aux_2 = aux_3;
                aux_3 = aux_3->der;
            }
        }
        //Busca el minimo de los maximos (siempre y cuando no haya encontrado un sucesor todavia)
        if (aux_3->der != nullptr && aux_3->valor == clave) {
            aux_3 = aux_3->der;
            while (aux_3->izq != nullptr) {
                aux_2 = aux_3;
                aux_3 = aux_3->izq;
            }
        }
        // El nodo a remover no tiene hijos
        if (aux_1 == aux_3) {
            //El nodo a remover es la raiz del arbol, sino....
            if (aux_1 == _raiz) {
                delete _raiz;
                _raiz = nullptr;
            } else {
                //Correcion de punteros
                if (aux_0->der == aux_1) {
                    aux_0->der = nullptr;
                } else {
                    aux_0->izq = nullptr;
                }
                delete aux_1;
            }
            // El nodo a remover tiene al menos un hijo
        } else {
            //Es la raiz del arbol
            if (_raiz->valor == aux_1->valor) {
                _raiz = aux_3;
            //No es la raiz del arbol
            } else {
                //Reasignar el sucesor con la raiz del removido
                if (aux_0->der == aux_1) {
                    aux_0->der = aux_3;
                } else {
                    aux_0->izq = aux_3;
                }
            }
            //El sucesor es hijo directo del removido
            if (aux_2 == aux_1) {
                //Por como se desarrolla el algoritmo, al no encontrar el sucesor con las propiedades del "maximo de los minimos", se descarta que el nodo a
                // remover tenga una rama izquierda, y por lo tanto el caso contrario seria el de "el minimo de los maximos", el cual no se deberia
                // reacomodar el puntero izquierdo del sucesor, ya que no habria una rama izquierda para hacer referencia.
                if (aux_1->valor > aux_3->valor) {
                    aux_3->der = aux_1->der;
                }
                //El sucesor no es hijo directo del removido
            } else {
                //El sucesor es el "maximo de los minimos"(ya esta re-asignado el sucesor con la raiz del removido)
                if (aux_1->valor > aux_3->valor) {
                    aux_2->der = aux_3->izq;
                    //El sucesor es el "minimo de los maximo"(si esto es asi, no existe rama izquierda al nodo a eliminar)
                } else {
                    aux_2->izq = aux_3->der;
                }
                //Reasigno los punteros del removido hacia el sucesor
                aux_3->izq = aux_1->izq;
                aux_3->der = aux_1->der;
            }
            delete aux_1;
        }
    }
    _cardinal--;
}


template<class T>
const T &Conjunto<T>::siguiente(const T &clave) {
    Nodo *aux_0 = _raiz;
    Nodo *aux_1 = _raiz;
    while (aux_1->valor != clave) {
        if (aux_1->valor < clave) {
            aux_0 = aux_1;
            aux_1 = aux_1->der;
        } else {
            aux_0 = aux_1;
            aux_1 = aux_0->izq;
        }
    }
    if (aux_1->der != nullptr) {
        aux_0 = aux_1->der;
        while (aux_0->izq != nullptr) {
            aux_0 = aux_0->izq;
        }
    }
    return aux_0->valor;
}

template<class T>
const T &Conjunto<T>::minimo() const {
    Nodo *aux = _raiz;
    while (aux->izq != nullptr) {
        aux = aux->izq;
    }
    return aux->valor;
}

template<class T>
const T &Conjunto<T>::maximo() const {
    Nodo *aux = _raiz;
    while (aux->der != nullptr) {
        aux = aux->der;
    }
    return aux->valor;
}

template<class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template<class T>
void Conjunto<T>::mostrar(std::ostream &) const {
    assert(false);
}

template<class T>
void Conjunto<T>::eliminarNodo(Conjunto::Nodo *nodo) {
        if (nodo->izq != nullptr) {
            eliminarNodo(nodo->izq);
        }
        if (nodo->der != nullptr) {
            eliminarNodo(nodo->der);
        }
        delete nodo;
        _cardinal--;
    }



template<class T>
Conjunto<T>::Nodo::Nodo(const T &v) : valor(v), izq(nullptr), der(nullptr) {}