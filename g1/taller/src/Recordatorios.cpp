#include <iostream>


using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha

class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes() const ;
    int dia() const ;
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(const Fecha& o) const ;
    #endif
    void incrementar_dia();
    #if EJ >=14 // Para ejercicio 14
    bool operator<(const Fecha& o) const ;
    #endif

  private:
    int mes_;
    int dia_;

};
Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia)   {}
int Fecha::mes() const {
    return mes_;
}
int Fecha::dia() const {
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << "" << f.dia() <<"/"<< f.mes() <<"";
    return os;
}

#if EJ >= 9
bool Fecha::operator==(const Fecha &o) const {
    bool igual_dia = this->dia_ == o.dia();
    bool igual_mes = this->mes_ == o.mes();
    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia() {
    if (dias_en_mes(mes_) < dia_ + 1){
        dia_ = 1;
        mes_++;
    }
    else{
        dia_++;
    }
}
#if EJ >= 14
bool Fecha::operator<(const Fecha &o) const {
    bool menor_dia = this->dia_ > o.dia();
    bool menor_mes = this->mes_ > o.mes();
    bool igual_mes = this->mes_ == o.mes();
    return (igual_mes && menor_dia) || menor_mes;
}
#endif




// Ejercicio 11, 12

class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario h);
        bool operator<(Horario h);

    private:
        int hora_;
        int min_;

};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora_ == h.hora();
    bool iguales_min = this->min_ == h.min();
    return igual_hora && iguales_min;
}

bool Horario::operator<(Horario h) {
    bool r = true;
    if (h.hora() < hora_){
        r = false;
    } else{
        if (h.hora() == hora_){
            if (h.min() < min_){
                r = false;
            }
        }
    }
    return r;
}


ostream& operator<<(ostream& os, Horario h) {
    os << "" << h.hora() <<":"<< h.min() <<"";
    return os;
}


// Ejercicio 13

using recordatorio = string;

class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, recordatorio r);
        recordatorio r();
        Fecha f();
        Horario h();

    private:
        recordatorio r_;
        Fecha f_;
        Horario h_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, recordatorio r) : f_(f), h_(h), r_(r) {}
string Recordatorio::r() {
    return r_;
}
Fecha Recordatorio::f() {
    return f_;
}
Horario Recordatorio::h() {
    return h_;
}

ostream& operator<<(ostream& os, Recordatorio r){
    os << "" << r.r() << " @ " << r.f() << " " << r.h() <<"";
    return os;
}


// Ejercicio 14

class Agenda{
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        vector<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        map<Fecha, vector<Recordatorio>> agenda_;
        Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial), agenda_(){}
void Agenda::agregar_recordatorio(Recordatorio rec) {
    agenda_[rec.f()].push_back(rec);
}
void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

vector<Recordatorio> Agenda::recordatorios_de_hoy() {
    vector<Recordatorio> r;
    //Copia los Recordatorios
    for (Recordatorio rec : agenda_[hoy_]) {
        r.push_back(rec);
    }
    //Insertion Sort para Recordatiros x hora
    for (int i = 0; i < r.size(); ++i) {
        if (r[i + 1].h() < r[i].h()) {
            if(i<r.size()-1) {
                int j = i + 1;
                while (j > 0 && r[j].h() < r[j - 1].h()) {
                    Recordatorio aux = r[j];
                    r[j] = r[j - 1];
                    r[j - 1] = aux;
                    j--;
                }
            }
        }
    }
    return r;
}
Fecha Agenda::hoy() {
    return hoy_;
}

ostream& operator<<(ostream& os, Agenda a){
    os << "" << a.hoy() << endl << "=====" << endl;
    vector<Recordatorio> rec = a.recordatorios_de_hoy();
    for (int i = 0; i < rec.size(); ++i) {
        os << rec[i] << endl;
    }
    return  os;
}




