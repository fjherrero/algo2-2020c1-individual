#include <utility>

using namespace std;

// Ejercicio 15

// Juego

using Pos = pair<int, int>;

char ARRIBA = '^';
char ABAJO = 'v'; 
char DERECHA = '<';
char IZQUIERDA = '>';

class Juego {
    public:
        Juego(uint casilleros, Pos pos_inicial);
        Pos posicion_jugador();
        uint turno_actual();
        void mover_jugador(char dir);
        void ingerir_pocion(uint movimientos, uint turnos);
    private:
        uint casilleros_;
        Pos pos_jugador_;
        uint turno_;
        vector<pair<uint, uint>> posiones_;
        uint efecto_posciones_;
  // Completar
};

Juego::Juego(uint casilleros, Pos pos_inicial) : casilleros_(casilleros), pos_jugador_(pos_inicial), turno_(0), posiones_(), efecto_posciones_(){}
Pos Juego::posicion_jugador() {
    return pos_jugador_;
}
uint Juego::turno_actual() {
    return turno_;
}
void Juego::mover_jugador(char dir) {
    if(turno_ != 0){
        if(dir == IZQUIERDA && 0 < posicion_jugador().first){
            pos_jugador_.first--;
        }
        if(dir == DERECHA && posicion_jugador().first < casilleros_ - 1){
            pos_jugador_.first++;
        }
        if(dir == ARRIBA && 0 < posicion_jugador().second){
            pos_jugador_.second--;
        }
        if(dir == ABAJO && posicion_jugador().second < casilleros_ - 1){
            pos_jugador_.second++;
        }
    } else{
        turno_++;
    }
}
void Juego::ingerir_pocion(uint movimientos, uint turnos) {
    posiones_.push_back({movimientos,turnos});
    efecto_posciones_ + movimientos;
}
























