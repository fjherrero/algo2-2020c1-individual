#include <iostream>

using namespace std;

using uint = unsigned int;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        int alto_;
        int ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho){};

uint Rectangulo::alto() {
    return alto_;
}

uint Rectangulo::ancho() {
    return ancho_;
}

float Rectangulo::area() {
    return ancho() * alto();
}

// Ejercicio 2

class Elipse {
    public:
        Elipse (uint r_a, uint r_b);
        uint r_a();
        uint r_b();
        float area();
        float PI = 3.14;
    private:
        int r_a_;
        int r_b_;
};

Elipse::Elipse(uint r_a, uint r_b) : r_a_(r_a), r_b_(r_b){}

uid_t Elipse::r_a() {
    return r_a_;
}

uid_t Elipse::r_b() {
    return r_b_;
}

float Elipse::area() {
    return r_a() * r_b() * PI;
}

// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    return r_.alto();
}

float Cuadrado::area() {
    return r_.alto() * r_.ancho();
}

// Ejercicio 4

class Circulo{
    public:
        Circulo(uint radio);
        uint radio();
        float area();
        float PI = 3.14;

    private:
        int radio_;
};

Circulo::Circulo(uint radio) : radio_(radio) {};

uint Circulo::radio() {
    return radio_;
}

float Circulo::area() {
    return radio() * radio() * PI;
}

// Clase Circulo


// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

ostream& operator<<(ostream& os, Elipse r) {
    os << "Elipse(" << r.r_a() << ", " << r.r_b() << ")";
    return os;
}


// Ejercicio 6

ostream& operator<<(ostream& os, Cuadrado r) {
    os << "Cuad(" << r.lado() <<")";
    return os;
}

ostream& operator<<(ostream& os, Circulo r) {
    os << "Circ(" << r.radio() <<")";
    return os;
}