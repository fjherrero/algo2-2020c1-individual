#include "string_map.h"

template<typename T>
string_map<T>::string_map() : _raiz(new Nodo()), _size(0) {}

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar)
        : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    aux_copia(d._raiz, _raiz);
    _size = d._size;
    return *this;
}

template<typename T>
string_map<T>::~string_map() {
    aux_destructor(_raiz);
    _size = 0;
}


template<typename T>
T &string_map<T>::operator[](const string &clave) {
    // COMPLETAR
}

template<typename T>
int string_map<T>::count(const string &clave) const {
    int res = 0;
    if (!empty()) {
        Nodo *n_aux = _raiz;
        int i = 0;
        buscarCoincidencia(clave, i, n_aux);
        if (n_aux->definicion != nullptr && i == clave.size()){
            res = 1;
        }
    }
    return  res;
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    return at(clave);
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo *n_aux = _raiz;
    int i = 0;
    buscarCoincidencia(clave, i, n_aux);
    return *(n_aux->definicion);
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    Nodo *n_aux = _raiz;
    int i = 0;
    aux_erase(clave,i, n_aux);
}

template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    bool res = true;
    if (size() > 0 ){
        res = false;
    }
    return res;
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &p) {
    if (count(p.first) == 1){
        Nodo *n_aux = _raiz;
        int i = 0;
        buscarCoincidencia(p.first, i, n_aux);
        delete n_aux->definicion;
        n_aux->definicion = nullptr;
    }
    Nodo *n_aux = _raiz;
    int i = 0;
    buscarCoincidencia(p.first, i, n_aux);
    if (i == 0){
        for (int j = 0; j < (p.first).size(); ++j) {
            int k = int(p.first[j]);
            n_aux->siguientes[k] = new Nodo();
            n_aux = n_aux->siguientes[k];
        }
        n_aux->definicion = new T(p.second);
    } else{
        if (i == (p.first).size()){
            n_aux->definicion = new T(p.second);
        } else{
            for (int j = i; j < (p.first).size(); ++j) {
                int k = int(p.first[j]);
                n_aux->siguientes[k] = new Nodo();
                n_aux = n_aux->siguientes[k];
            }
            n_aux->definicion = new T(p.second);
        }
    }
    _size++;
}


template<typename T>
void string_map<T>::buscarCoincidencia(const string &clave, int &indice, string_map::Nodo *&nodo) const {
    if (indice < clave.size()) {
        int i = int(clave[indice]);
        if ((nodo->siguientes)[i] != nullptr) {
            nodo = (nodo->siguientes)[i];
            indice++;
            buscarCoincidencia(clave, indice, nodo);
        }
    }
}

template<typename T>
void string_map<T>::aux_copia(const Nodo *nodo_0, Nodo *&nodo_c) {
    if (nodo_0->definicion != nullptr){
        nodo_c->definicion = new T(*(nodo_0->definicion));
    }
    for (int i = 0; i < (nodo_0->siguientes).size(); i++) {
        if(nodo_0->siguientes[i] != nullptr){
            nodo_c->siguientes[i] = new Nodo();
            aux_copia(nodo_0->siguientes[i], nodo_c->siguientes[i]);
        }
    }
}

template<typename T>
void string_map<T>::aux_erase(const string &clave, int &indice, string_map::Nodo *&nodo) {
    if (indice < clave.size()) {
        int i = int(clave[indice]);
        if ((nodo->siguientes)[i] != nullptr) {
            Nodo *nodo_rec = (nodo->siguientes)[i];
            indice++;
            aux_erase(clave, indice, nodo_rec);
            bool unico = true;
            for (Nodo *n : nodo_rec->siguientes){
                if (n != nullptr){
                    unico = false;
                }
            }
            if (unico){
                delete nodo_rec;
                nodo->siguientes[i] = nullptr;
            }
        }
    } else{
        delete nodo->definicion;
        nodo->definicion = nullptr;
        _size--;
    }
}

template<typename T>
void string_map<T>::aux_destructor(string_map::Nodo *&nodo) {
    for(int i = 0; i < (nodo->siguientes).size(); i++){
        if((nodo->siguientes)[i] != nullptr){
            aux_destructor((nodo->siguientes)[i]);
        }
    }
    if(nodo->definicion != nullptr) {
        delete nodo->definicion;
        nodo->definicion = nullptr;
    }
    delete nodo;
    nodo = nullptr;
}


//Constructor de la struct Nodo
template<typename T>
string_map<T>::Nodo::Nodo(): definicion(nullptr), siguientes(256, nullptr) {}

template<typename T>
string_map<T>::Nodo::Nodo(const T *def): definicion(def), siguientes(256, nullptr) {}

template<typename T>
string_map<T>::Nodo::Nodo(const T &def): definicion(def), siguientes(256, nullptr) {}
